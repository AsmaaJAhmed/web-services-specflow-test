﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace RESTServiceApplication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TradeServiceImpl" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TradeServiceImpl.svc or TradeServiceImpl.svc.cs at the Solution Explorer and start debugging.
    public class TradeServiceImpl : ITradeServiceImpl
    {
        public List<ResponseData> GetTradeList()
        {
            var dt = (new DatabaseHelper()).GetResultSet("SELECT * FROM Trades");
            var tradeList = from dr in dt.AsEnumerable()
                select new ResponseData()
                {
                    Reference = dr["Reference"].ToString(),
                    TradeDate = dr["TradeDate"].ToString(),
                    Volume = Convert.ToDecimal(dr["Volume"].ToString()),
                };

            return tradeList.ToList();
        }

        public ResponseData GetTradeByReference(string reference)
        {
            var dt = (new DatabaseHelper()).GetResultSet($"SELECT * FROM Trades WHERE Reference = '{reference}'");
            if (dt.Rows.Count <= 0) return null;

            var dr = dt.Rows[0];
            return new ResponseData()
            {
                Reference = dr["Reference"].ToString(),
                TradeDate = dr["TradeDate"].ToString(),
                Volume = Convert.ToDecimal(dr["Volume"].ToString()),
            };
        }

        public ResponseData AddTrade(RequestData requestData)
        {
            var dt = (new DatabaseHelper()).GetResultSet("SELECT * FROM Trades");

            var lastIdinDatatable = dt.Rows[dt.Rows.Count - 1]["TradeId"].ToString();
            var id = Convert.ToInt32(lastIdinDatatable) + 1;

            var insertCommand =
                $"INSERT INTO Trades VALUES({id}, '{requestData.Reference}', '{requestData.TradeDate}', {requestData.Volume})";


            (new DatabaseHelper()).SqlExecute(insertCommand);

            return new ResponseData
            {
                Reference = requestData.Reference,
                TradeDate = requestData.TradeDate,
                Volume = requestData.Volume,
            };
        }

        public void UpdateTrade(RequestData updatedResponseData, string reference)
        {
            var updateCommand =
                $"UPDATE Trades SET Reference = '{updatedResponseData.Reference}', TradeDate = '{updatedResponseData.TradeDate}', " +
                $"Volume ={updatedResponseData.Volume} WHERE Reference = '{reference}'";

            (new DatabaseHelper()).SqlExecute(updateCommand);
        }

        public void DeleteTrade(string reference)
        {
            (new DatabaseHelper()).SqlExecute($"DELETE FROM Trades WHERE Reference = '{reference}'");
        }

        //public void PatchTrade(RequestData updatedResponseData, string reference)
        //{
        //    var dt = (new DatabaseHelper()).GetResultSet($"SELECT * FROM Trades WHERE Reference = '{reference}'");
        //}
    }
}