﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace RESTServiceApplication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITradeServiceImpl" in both code and config file together.
    [ServiceContract]
    public interface ITradeServiceImpl
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Trades", BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Xml)]
        List<ResponseData> GetTradeList();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Trades/{id}", BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Xml)]
        ResponseData GetTradeByReference(string id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/AddTrade", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml)]
        ResponseData AddTrade(RequestData tradeReference);


        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "/UpdateTrade/{reference}", BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml)]
        void UpdateTrade(RequestData updatedResponseData, string reference);

        //[OperationContract]
        //[WebInvoke(Method = "PATCH", UriTemplate = "/PatchTrade/{reference}", BodyStyle = WebMessageBodyStyle.Bare,
        //  RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml)]
        //void PatchTrade(RequestData updatedResponseData, string reference);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/DeleteTrade/{reference}", Method = "DELETE", ResponseFormat = WebMessageFormat.Xml)]
        void DeleteTrade(string reference);

    }
}