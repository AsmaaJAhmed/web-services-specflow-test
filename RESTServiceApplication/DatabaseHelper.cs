﻿using System.Data;
using System.Data.SqlClient;

namespace RESTServiceApplication
{
    public class DatabaseHelper
    {
        private static string ConnectionString => System.Configuration.ConfigurationManager.ConnectionStrings["cn"].ConnectionString;

        public DataTable GetResultSet(string sql)
        {
            var dt = new DataTable();
            using (var da = new SqlDataAdapter(sql, ConnectionString))
            {
                da.Fill(dt);
            }
            return dt;
        }

        public void SqlExecute(string sql)
        {
            using (var cmd = new SqlCommand(sql, new SqlConnection(ConnectionString)))
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
        }
    }
}