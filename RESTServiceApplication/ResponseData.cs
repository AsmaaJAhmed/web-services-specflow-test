﻿using System.Runtime.Serialization;

namespace RESTServiceApplication
{
    [DataContract(Namespace = "http://localhost:57577")]
    public class RequestData
    {
        [DataMember(Order = 0)]
        public string Reference { get; set; }
        [DataMember(Order = 1)]
        public decimal Volume { get; set; }
        [DataMember(Order = 2)]
        public string TradeDate { get; set; }
    }


    [DataContract]
    public class ResponseData
    {
        [DataMember(Order = 0)]
        public string Reference { get; set; }
        [DataMember(Order = 1)]
        public decimal Volume { get; set; }
        [DataMember(Order = 2)]
        public  string TradeDate { get; set; }
    }
}