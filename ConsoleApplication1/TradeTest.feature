﻿Feature: TradeTest

# Uses EXACT string comparison

# GET
Scenario: 010 Get all trades in Database 
	Given My service "TradeServiceImpl.svc" is running
	When I get all saved trades
	Then the response is successful 
	And the trades match the trades in the expected xml file "ExpectedTrades.xml"

# GET
Scenario: 020 Get specific trade
	Given My service "TradeServiceImpl.svc" is running
	When I get trade with reference "DB-Trade-04"
	Then the response is successful 
	And the trade matches the trade in the expected xml file "ExpectedTradeByReference.xml"

#POST
Scenario: 030 Create a new trade 
	Given My service "TradeServiceImpl.svc" is running
	When I create a trade from the following xml file "AddTradeToDb.xml"
	Then the trade is created successfully 


# PUT
Scenario: 040 Update an existing trade 
	Given My service "TradeServiceImpl.svc" is running
	When I update trade with reference "DB-Trade-04" using the following xml file "UpdateSingleTrade.xml"
	Then the trade is updated successfully

# PATCH
# Scenario: 050 

# DELETE
Scenario: 050 Delete trade
	Given My service "TradeServiceImpl.svc" is running
	When I delete trade with reference "DB-Trade-01"
	Then the trade is deleted successfully

# Uses XML Comparer 
#Scenario: 020 Get all trades in Database
#	Given My service "TradeServiceImpl.svc" is running
#	When I get all saved trades
#	Then the response is successful 
#	And the trades match the trades in the expected xml file "ExpectedTrades.xml"
