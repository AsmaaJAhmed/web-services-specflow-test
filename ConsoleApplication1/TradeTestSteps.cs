﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using TechTalk.SpecFlow;
using NUnit.Framework;


namespace ConsoleApplication1
{
    [Binding]
    public sealed class TradeTestSteps
    {
        private const string ServerUrl = "ServerURL";
        private const string ServerResponse = "serverResponse";
        private const string WebHttpStatusCode = "StatusCode";

        private const string folderPath =
            "C:\\Users\\asmaa\\documents\\visual studio 2015\\Projects\\RESTServiceApplication\\ConsoleApplication1\\XML\\";

        #region Context Server URL

        private void AddServerUrlToContext(string url)
        {
            ScenarioContext.Current.Set(url, ServerUrl);
        }

        private string GetServerUrlfromContext()
        {
            if (!ScenarioContext.Current.ContainsKey(ServerUrl))
                Assert.Fail("No prior mention of server url");

            var contx = ScenarioContext.Current.Get<string>(ServerUrl);
            return contx;
        }

        #endregion

        #region Context Server Response Data

        private void AddServerResponseToContext(string serverResponse)
        {
            ScenarioContext.Current.Set(serverResponse, ServerResponse);
        }

        private string GetServerResponseFromContext()
        {
            if (!ScenarioContext.Current.ContainsKey(ServerResponse))
                Assert.Fail("No prior mention of server url");

            var contx = ScenarioContext.Current.Get<string>(ServerResponse);
            return contx;
        }

        #endregion

        #region Context Server Status Code

        private void AddStatusCodeToContext(HttpStatusCode statusCode)
        {
            ScenarioContext.Current.Set(statusCode, WebHttpStatusCode);
        }

        private HttpStatusCode GetStatusCodeFromContext()
        {
            if (!ScenarioContext.Current.ContainsKey(ServerResponse))
                Assert.Fail("No prior mention of server url");

            var contx = ScenarioContext.Current.Get<HttpStatusCode>(WebHttpStatusCode);
            return contx;
        }

        #endregion

        [Given(@"My service ""(.*)"" is running")]
        public void GivenMyServiceIsRunning(string server)
        {
            var url = "http://localhost:57577/" + server;

            var req = WebRequest.Create(url) as HttpWebRequest;
            var response = req?.GetResponse();

            if (response == null)
            {
                Assert.Fail("Web request failed to get a response. Check URL");
            }
            else
            {
                response.Close();
                AddServerUrlToContext(url);
            }
        }

        [When(@"I get all saved trades")]
        public void WhenIGetAllSavedTrades()
        {
            GetTrades("/trades");
        }

        [When(@"I get trade with reference ""(.*)""")]
        public void WhenIGetTradeWithReference(string tradeReference)
        {
            GetTrades(uriTemplate: $"/trades/{tradeReference}");
        }


        [When(@"I create a trade from the following xml file ""(.*)""")]
        public void WhenICreateATradeFromTheFollowingXmlFile(string xmlFile)
        {
            var uri = GetServerUrlfromContext() + "/AddTrade";

            SendHttpWebRequest(uri, xmlFile, "POST");
        }


        [When(@"I update trade with reference ""(.*)"" using the following xml file ""(.*)""")]
        public void WhenIUpdateTradeWithReferenceUsingTheFollowingXmlFile(string tradeReference, string xmlFile)
        {
            var uri = GetServerUrlfromContext() + $"/UpdateTrade/{tradeReference}";
            SendHttpWebRequest(uri, xmlFile, "PUT");
        }

        [When(@"I delete trade with reference ""(.*)""")]
        public void WhenIDeleteTradeWithReference(string tradeReference)
        {
            try
            {
                var request =
                    (HttpWebRequest)
                    WebRequest.Create(GetServerUrlfromContext() + $"/DeleteTrade/{tradeReference}");

                request.Method = "DELETE";
                var requestStream = request.GetRequestStream();

                requestStream.Close();
                var response = (HttpWebResponse) request.GetResponse();
                AddStatusCodeToContext(response.StatusCode);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "/r/n" + ex.StackTrace);
            }
        }


        // Status Code Check
        [Then(@"the trade is created successfully")]
        [Then(@"the trade is deleted successfully")]
        [Then(@"the trade is updated successfully")]
        public void ThenTheTradeIsUpdatedSuccessfully()
        {
            var statusCode = GetStatusCodeFromContext();
            Assert.IsTrue(statusCode == HttpStatusCode.OK);
        }


        // ResponseData check
        [Then(@"the response is successful")]
        public void ThenTheResponseIsSuccessful()
        {
            var response = GetServerResponseFromContext();
            Assert.IsTrue(response.Length > 0, response);
        }


        // Exact string comparison
        [Then(@"the trade matches the trade in the expected xml file ""(.*)""")]
        [Then(@"the trades match the trades in the expected xml file ""(.*)""")]
        public void ThenTheTradesMatchTheTradesInTheExpectedXmlFile(string xmlFile)
        {
            var enc = Encoding.GetEncoding(1252);
            var actualResults = GetServerResponseFromContext();

            var streamReader = new StreamReader(folderPath + xmlFile, enc);
            var expectedResults = streamReader.ReadToEnd();


            Assert.IsTrue(actualResults.Equals(expectedResults));
        }

        #region Private Methods

        private void GetTrades(string uriTemplate)
        {
            try
            {
                const string method = "GET";
                var uri = GetServerUrlfromContext() + uriTemplate;
                var req = WebRequest.Create(uri) as HttpWebRequest;
                if (req == null) return;
                req.KeepAlive = false;
                req.Method = method.ToUpper();
                var resp = req.GetResponse() as HttpWebResponse;
                var enc = Encoding.GetEncoding(1252);
                var loResponseStream =
                    new StreamReader(resp.GetResponseStream(), enc);
                var response = loResponseStream.ReadToEnd();
                loResponseStream.Close();
                resp.Close();
                AddServerResponseToContext(response);
            }
            catch (Exception ex)
            {
                Assert.Fail($"Error: {ex.Message}");
            }
        }

        private void SendHttpWebRequest(string uri, string xmlFile, string method)
        {
            try
            {
                var request =
                    (HttpWebRequest)
                    WebRequest.Create(uri);
                var content = (File.OpenText(folderPath + xmlFile)).ReadToEnd();

                var bytes = Encoding.ASCII.GetBytes(content);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = method.ToUpper();
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Flush();
                requestStream.Close();

                var resp = (HttpWebResponse) request.GetResponse();

                AddStatusCodeToContext(resp.StatusCode);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + "/r/n" + ex.StackTrace);
            }
        }

        #endregion
    }
}