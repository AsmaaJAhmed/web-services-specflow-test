﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConsoleApplication1.ServiceReference1 {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Trade", Namespace="http://schemas.datacontract.org/2004/07/RESTServiceApplication")]
    [System.SerializableAttribute()]
    public partial class Trade : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ReferenceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal VolumeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TradeDateField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Reference {
            get {
                return this.ReferenceField;
            }
            set {
                if ((object.ReferenceEquals(this.ReferenceField, value) != true)) {
                    this.ReferenceField = value;
                    this.RaisePropertyChanged("Reference");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal Volume {
            get {
                return this.VolumeField;
            }
            set {
                if ((this.VolumeField.Equals(value) != true)) {
                    this.VolumeField = value;
                    this.RaisePropertyChanged("Volume");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=2)]
        public string TradeDate {
            get {
                return this.TradeDateField;
            }
            set {
                if ((object.ReferenceEquals(this.TradeDateField, value) != true)) {
                    this.TradeDateField = value;
                    this.RaisePropertyChanged("TradeDate");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.ITradeServiceImpl")]
    public interface ITradeServiceImpl {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITradeServiceImpl/GetTradeList", ReplyAction="http://tempuri.org/ITradeServiceImpl/GetTradeListResponse")]
        ConsoleApplication1.ServiceReference1.Trade[] GetTradeList();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITradeServiceImpl/GetTradeList", ReplyAction="http://tempuri.org/ITradeServiceImpl/GetTradeListResponse")]
        System.Threading.Tasks.Task<ConsoleApplication1.ServiceReference1.Trade[]> GetTradeListAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITradeServiceImpl/GetTradeByReference", ReplyAction="http://tempuri.org/ITradeServiceImpl/GetTradeByReferenceResponse")]
        ConsoleApplication1.ServiceReference1.Trade GetTradeByReference(string id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITradeServiceImpl/GetTradeByReference", ReplyAction="http://tempuri.org/ITradeServiceImpl/GetTradeByReferenceResponse")]
        System.Threading.Tasks.Task<ConsoleApplication1.ServiceReference1.Trade> GetTradeByReferenceAsync(string id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITradeServiceImpl/AddTrade", ReplyAction="http://tempuri.org/ITradeServiceImpl/AddTradeResponse")]
        void AddTrade(ConsoleApplication1.ServiceReference1.Trade newTrade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITradeServiceImpl/AddTrade", ReplyAction="http://tempuri.org/ITradeServiceImpl/AddTradeResponse")]
        System.Threading.Tasks.Task AddTradeAsync(ConsoleApplication1.ServiceReference1.Trade newTrade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITradeServiceImpl/UpdateTrade", ReplyAction="http://tempuri.org/ITradeServiceImpl/UpdateTradeResponse")]
        void UpdateTrade(ConsoleApplication1.ServiceReference1.Trade updatedTrade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITradeServiceImpl/UpdateTrade", ReplyAction="http://tempuri.org/ITradeServiceImpl/UpdateTradeResponse")]
        System.Threading.Tasks.Task UpdateTradeAsync(ConsoleApplication1.ServiceReference1.Trade updatedTrade);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITradeServiceImpl/DeleteTrade", ReplyAction="http://tempuri.org/ITradeServiceImpl/DeleteTradeResponse")]
        void DeleteTrade(string id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITradeServiceImpl/DeleteTrade", ReplyAction="http://tempuri.org/ITradeServiceImpl/DeleteTradeResponse")]
        System.Threading.Tasks.Task DeleteTradeAsync(string id);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ITradeServiceImplChannel : ConsoleApplication1.ServiceReference1.ITradeServiceImpl, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class TradeServiceImplClient : System.ServiceModel.ClientBase<ConsoleApplication1.ServiceReference1.ITradeServiceImpl>, ConsoleApplication1.ServiceReference1.ITradeServiceImpl {
        
        public TradeServiceImplClient() {
        }
        
        public TradeServiceImplClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public TradeServiceImplClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TradeServiceImplClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TradeServiceImplClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public ConsoleApplication1.ServiceReference1.Trade[] GetTradeList() {
            return base.Channel.GetTradeList();
        }
        
        public System.Threading.Tasks.Task<ConsoleApplication1.ServiceReference1.Trade[]> GetTradeListAsync() {
            return base.Channel.GetTradeListAsync();
        }
        
        public ConsoleApplication1.ServiceReference1.Trade GetTradeByReference(string id) {
            return base.Channel.GetTradeByReference(id);
        }
        
        public System.Threading.Tasks.Task<ConsoleApplication1.ServiceReference1.Trade> GetTradeByReferenceAsync(string id) {
            return base.Channel.GetTradeByReferenceAsync(id);
        }
        
        public void AddTrade(ConsoleApplication1.ServiceReference1.Trade newTrade) {
            base.Channel.AddTrade(newTrade);
        }
        
        public System.Threading.Tasks.Task AddTradeAsync(ConsoleApplication1.ServiceReference1.Trade newTrade) {
            return base.Channel.AddTradeAsync(newTrade);
        }
        
        public void UpdateTrade(ConsoleApplication1.ServiceReference1.Trade updatedTrade) {
            base.Channel.UpdateTrade(updatedTrade);
        }
        
        public System.Threading.Tasks.Task UpdateTradeAsync(ConsoleApplication1.ServiceReference1.Trade updatedTrade) {
            return base.Channel.UpdateTradeAsync(updatedTrade);
        }
        
        public void DeleteTrade(string id) {
            base.Channel.DeleteTrade(id);
        }
        
        public System.Threading.Tasks.Task DeleteTradeAsync(string id) {
            return base.Channel.DeleteTradeAsync(id);
        }
    }
}
