﻿using System;
using System.IO;
using System.Net;

namespace ConsoleApplication1
{
    class Program
    {
        public static readonly string Path = System.IO.Path.Combine(Environment.CurrentDirectory, @"XML\");
        //"C:\\Users\\asmaa\\documents\\visual studio 2015\\Projects\\RESTServiceApplication\\ConsoleApplication1\\XML\\";

        static void Main(string[] args)
        {
            /*
             * The WebInvoke/WebGet attributes are used when you expose your service via WebHttpBinding (REST Style) 
             * In order to access the method via SOAP add [OperationContract] attribute and expose an endpoint via basicHttpBinding
             * 
             * Access the service in a REST Style then use the HttpWebRequest class to create your request rather than adding a Service Reference 
             * 
             */


            //WebHttpBinding binding = new WebHttpBinding();
            //EndpointAddress address = new EndpointAddress("http://localhost:57577/TradeServiceImpl.svc");

            //TradeServiceImplClient serviceClient = new TradeServiceImplClient(binding, address);
            //// TradeServiceImplClient client = new TradeServiceImplClient("webHttpBinding_ITradeServiceImpl");
            ////client.Open();

            //serviceClient.Endpoint.Behaviors.Add(new WebHttpBehavior());

            //var xd = serviceClient.GetTradeList();

            //Console.WriteLine(client.GetTradeByReference("test-Specflow"));
            //client.Close();

            // absoloute path? IIS endpoint
            //WebChannelFactory<ITradeServiceImpl> cf =
            //    new WebChannelFactory<ITradeServiceImpl>(
            //        new Uri("http://localhost:57577/TradeServiceImpl.svc"));


            //ITradeServiceImpl client = cf.CreateChannel();
            //var d = client.GetTradeByReference("44");
            //foreach (Trade e in d)
            //{
            //    Console.WriteLine($"Trade Ref:{e.Reference}, Trade Date:{e.TradeDate}, Trade Volume: {e.Volume}");
            //}

            ////Add new user
            //client.AddTrade(new Trade() { Reference = "11", TradeDate = "01/02/2018", Volume = (decimal)245.1 });

            //Console.WriteLine("&&&&&& After adding new trade &&&&&&");

            ////Load all the Employee from the server and display
            //foreach (Trade e in client.GetTradeList())
            //{
            //    Console.WriteLine($"Trade Ref:{e.Reference}, Trade Date:{e.TradeDate}, Trade Volume: {e.Volume}");
            //}
            //Console.ReadLine();


            //do
            //{
            //    try
            //    {
            //        var Method = "GET";

            //        var uri = "http://localhost:57577/TradeServiceImpl.svc/trades";

            //        var req = WebRequest.Create(uri) as HttpWebRequest;
            //        var isActive = req.GetResponse();
            //        if (isActive == null)
            //        {
            //            Console.WriteLine("could not connect to server please check URL");
            //        }
            //        req.KeepAlive = false;
            //        req.Method = Method.ToUpper();

            //        var resp = req.GetResponse() as HttpWebResponse;

            //        var enc = System.Text.Encoding.GetEncoding(1252);
            //        var loResponseStream =
            //            new StreamReader(resp.GetResponseStream(), enc);

            //        var Response = loResponseStream.ReadToEnd();

            //        loResponseStream.Close();
            //        resp.Close();
            //        Console.WriteLine(Response);
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex.Message.ToString());
            //    }

            //    Console.WriteLine();
            //    Console.WriteLine("Do you want to continue?");
            //} while (Console.ReadLine().ToUpper() == "Y");

            //try
            //{
            //    string url =
            //        "http://localhost:57577/TradeServiceImpl.svc/AddTrade";

            //    // declare ascii encoding
            //    ASCIIEncoding encoding = new ASCIIEncoding();
            //    string strResult = string.Empty;
            //    // sample xml sent to Service & this data is sent in POST
            //    var postData = (File.OpenText(Path + "TradeToAdd.xml")).ReadToEnd();
            //    // string postData = content.ToString();
            //    // convert xmlstring to byte using ascii encoding
            //    byte[] data = encoding.GetBytes(postData);
            //    // declare httpwebrequet wrt url defined above
            //    HttpWebRequest webrequest = (HttpWebRequest) WebRequest.Create(url);
            //    // set method as post
            //    webrequest.Method = "POST";
            //    // set content type
            //    webrequest.ContentType = "application/x-www-form-urlencoded";
            //    // set content length
            //    webrequest.ContentLength = data.Length;
            //    // get stream data out of webrequest object
            //    Stream newStream = webrequest.GetRequestStream();
            //    newStream.Write(data, 0, data.Length);
            //    newStream.Close();
            //    // declare & read response from service
            //    WebResponse webresponse = webrequest.GetResponse();
            //}
            //catch (Exception ex)
            //{
            //}
            
            //try
            //{
            // to think about:
            // using xml to insert but method takes in a trade object 

            //WebRequest request = WebRequest.Create("http://localhost:57577/TradeServiceImpl.svc/AddTrade");
            //request.Method = "POST";
            // var content = (File.OpenText(Path + "TradeToAdd.xml")).ReadToEnd();
            //byte[] byteArray = Encoding.ASCII.GetBytes(content);
            //request.ContentType = "text/xml";
            //request.ContentLength = byteArray.Length;
            //Stream dataStream = request.GetRequestStream();
            //dataStream.Write(byteArray, 0, byteArray.Length);
            //dataStream.Close();
            //WebResponse response = request.GetResponse();
            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            //dataStream = response.GetResponseStream();
            //StreamReader reader = new StreamReader(dataStream);
            //string responseFromServer = reader.ReadToEnd();
            ////Console.WriteLine(responseFromServer);

            //reader.Close();
            //dataStream.Close();
            //response.Close();


            // Update
            try
            {
                //string tradeReference = "testPost222";
                //   ASCIIEncoding encoding = new ASCIIEncoding();

                HttpWebRequest request =
                    (HttpWebRequest)
                    WebRequest.Create("http://localhost:57577/TradeServiceImpl.svc/UpdateTrade/DB-Trade-04" /*+ tradeReference*/);
                var content = (File.OpenText(Path + "UpdateSingleTrade.xml")).ReadToEnd();
                var bytes = System.Text.Encoding.ASCII.GetBytes(content);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "PUT";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Flush();
                requestStream.Close();
                var x = request.GetResponse();
                Console.WriteLine(x.IsFromCache + x.GetResponseStream().ToString());


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.StackTrace);
                Console.WriteLine(ex.InnerException);
            }


            // Delete 
            try
            {
             
                HttpWebRequest request =
                    (HttpWebRequest)
                    WebRequest.Create("http://localhost:57577/TradeServiceImpl.svc/DeleteTrade/trade-1" /*+ tradeReference*/);
               // var content = (File.OpenText(Path + "Trade44ToUpdate.xml")).ReadToEnd();
               // var bytes = System.Text.Encoding.ASCII.GetBytes(content);
               // request.ContentType = "text/xml; encoding='utf-8'";
               // request.ContentLength = bytes.Length;
                request.Method = "DELETE";
                Stream requestStream = request.GetRequestStream();
               // requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Flush();
                requestStream.Close();
                var x = request.GetResponse();
                Console.WriteLine(x.IsFromCache + x.GetResponseStream().ToString());


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.StackTrace);
                Console.WriteLine(ex.InnerException);
            }

            //    string groupMsg = sb1.ToString();
            //    bytes = System.Text.Encoding.ASCII.GetBytes(groupMsg);
            //    request.ContentType = "text/xml; encoding='utf-8'";
            //    request.ContentLength = bytes.Length;
            //    request.Method = "POST";
            //    Stream requestStream = request.GetRequestStream();
            //    requestStream.Write(bytes, 0, bytes.Length);
            //    requestStream.Flush();
            //    requestStream.Close();
            //    HttpWebResponse response;
            //    response = (HttpWebResponse)request.GetResponse();
            //    if (response.StatusCode == HttpStatusCode.OK)
            //    {
            //        Stream responseStream = response.GetResponseStream();
            //        string responseStr = new StreamReader(responseStream).ReadToEnd();
            //        Console.WriteLine(responseStr);
            //    }
            //}
            //catch (WebException ex)
            //{
            //    using (WebResponse response = ex.Response)
            //    {
            //        HttpWebResponse httpResponse = (HttpWebResponse)response;
            //        Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
            //        using (Stream data = response.GetResponseStream())
            //        using (var reader = new StreamReader(data))
            //        {
            //            string text = reader.ReadToEnd();
            //            Console.WriteLine(text);
            //        }
            //    }

            //    Console.WriteLine(ex.Message.ToString() + "\r\n" + ex.StackTrace.ToString() );
            //}

        }
    }
}